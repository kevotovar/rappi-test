
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

angular.module('cube-app', []).controller('CubeController', function($scope, $http){
    $scope.cube = '';
    $scope.size = 0;
    $scope.query = '';
    createCube = function(){
        $http.post('/cube/create', {
            size:this.size
        }).then(function(response) {
            $scope.cube = response.cube;
            $scope.size = response.size;
        });
    }
    $scope.query = function(){

    }
});