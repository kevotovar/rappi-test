<?php

namespace App;

class Cube
{
    private $cube = [];
    private $size = 0;
    
    // Creamos el cubo
    public function createCube($size){
        $this->cube = [];
        $this->size = $size;
        for($j = 0; $j < $size; $j++){
            $karray = [];
            for($k = 0; $k < $size; $k++){
                $larray = [];
                for($l = 0; $l < $size; $l++){
                    array_push($larray, 0);
                }
                array_push($karray,$larray);
            }
            array_push($this->cube,$karray);
        }
    }

    // Asignamos los datos que estan persistentes
    public function setCubeData(array $array){
        $this->cube = $array;
    }

    // Regresamos los valores del cubo
    public function getCubeData(){
        return $this->cube;
    }

    public function getCubeSize(){
        return $this->size;
    }

    //Validamos si la query es correcta
    private function validQuery($x, $y, $z){
        if ($x < 1 || $y < 1 || $z < 1){
            throw new Exception('El valor del query no puede ser 0');
        }
        if ($x > $this->size || $y > $this->size || $z > $this->size){
            throw new Exception('El valor del query no puede ser mayor al tamaño');
        }
        return true;
    }

    // Query para asignarle un valor 
    public function updateQuery($x, $y, $z, $value){
        if ($this->validQuery($x, $y, $z)){
            $this->cube[$x-1][$y-1][$z-1] = $value;
        }
    }

    public function runQuery($x1, $y1, $z1, $x2, $y2, $z2){
        if($this->validQuery($x1, $y1, $z1) && $this->validQuery($x2, $y2, $z2)){
            $sum = 0;
            for($ji = $query[1] - 1; $ji < $query[4]; $ji++){
                for($k = $query[2] - 1; $k < $query[5]; $k++){
                    for($l = $query[3] - 1; $l < $query[6]; $l++){
                        $sum = $sum + $cube[$ji][$k][$l];
                    }
                }
            }
            return $sum;
        }
    }
}
