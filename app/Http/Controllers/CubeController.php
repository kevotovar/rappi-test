<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cube;

class CubeController extends Controller
{
    // Obtenemos la primer vista del cubo
    public function index(){
        return view('cube');
    }

    public function createCube(Request $request){
        $data = $request->all();
        dd($data);
        $cube = new Cube();
        $cube->createCube($data['size']);
        return response()->json(array('cube' => $cube->getCubeData(), 'size' => $cube->getCubeSize()));
    }

    public function makeQuery(Request $request){

    }

}
